#!/usr/bin/env bash

# exit on any error
set -ex

# DEFINE VARS
OS_PACKAGES="less ffmpeg libmagickcore-6.q16-6-extra libsmbclient"
OS_PACKAGES_TEMP="libbz2-dev libc-client-dev libkrb5-dev libsmbclient-dev"

# NC23 apporder + files_mindmap not ready yet
NC_APPS="bookmarks calendar contacts deck files_accesscontrol
files_antivirus files_automatedtagging files_downloadactivity files_markdown
keeweb metadata news notes tasks twofactor_totp files_retention richdocuments
richdocumentscode notify_push maps"

# patch entrypoint avoiding php to store bytecode as root user in tmpfs and
# blocking www-data user from writing bytecode objects to fs
patch -p1 /entrypoint.sh /usr/local/misc/entrypoint.patch

# enable http2 module
a2enmod http2

### install additional libraries + tools ###
chmod +x /usr/local/bin/update_nc_apps.sh \
	 /usr/local/bin/wait_nc_available.sh

apt-get update
apt-get install -y --no-install-recommends \
   ${OS_PACKAGES}

### install additional php modules ###
apt-get install -y --no-install-recommends \
   ${OS_PACKAGES_TEMP}

# php-imap
docker-php-ext-configure imap --with-kerberos --with-imap-ssl
docker-php-ext-install imap
docker-php-ext-enable imap
# php-bz2
docker-php-ext-install bz2
docker-php-ext-enable bz2
# smbclient (use pecl)
pecl install smbclient
docker-php-ext-enable smbclient


apt-get purge -y \
   ${OS_PACKAGES_TEMP}


### install nextcloud apps ###
# make apps not stored in apps_custom
cd /usr/src/nextcloud/config
mv apps.config.php apps.config.php_orig
# create temporary nextcloud installation
cd /usr/src/nextcloud
php occ maintenance:install \
   --admin-pass=verysecure \
   --data-dir=/tmp/nc_data
# install NC apps
for APP in ${NC_APPS} ; do
  php occ app:install --keep-disabled -vvv ${APP}
done


# restore saved config and cleanup
cd /usr/src/nextcloud/config
mv apps.config.php_orig apps.config.php
rm config.php
rm -rf /tmp/nc_data /usr/src/nextcloud/data/*

### self cleanup ###
apt-get clean -y
rm -rf /var/lib/apt/lists/*
rm -rf /usr/local/misc
rm -f $0
