#!/bin/bash

set -eu

echo "Start checking for ${URL_TO_CHECK} up!"

while ! curl --fail -w %{http_code} ${URL_TO_CHECK} 1>&2 ; do 
  echo "Waiting for ${URL_TO_CHECK} up!" 
  sleep 5
done

echo "SUCCESS ${URL_TO_CHECK} is up!"

