#!/bin/bash

set -eu

NC_WWW_DATA="/var/www/html"

run_as() {
    if [ "$(id -u)" = 0 ]; then
        su -p www-data -s /bin/sh -c "$1"
    else
        sh -c "$1"
    fi
}



echo "Updating Apps"
run_as 'php /var/www/html/occ app:update --all' 

echo "Run Upgrade 1st"
run_as 'php /var/www/html/occ upgrade'

echo "Run Upgrade 2nd"
run_as 'php /var/www/html/occ upgrade'


