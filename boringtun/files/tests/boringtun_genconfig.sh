#!/usr/bin/env bash

SCRIPT=$(readlink -f $0)

# make sure environment is set otherwise exit
if [[ ! -v boringtun_wg1_ci_client || ! -v WGVOLUMEMNT ]]; then
  echo "ERROR: ${SCRIPT} please make sure environment boringtun_wg1_ci_client, WGVOLUMEMNT is set"
  exit 1
fi

echo "* ${SCRIPT} Create config wg1.conf"
echo "${boringtun_wg1_ci_client}" | base64 -d >  ${WGVOLUMEMNT}/wg1.conf

echo "* ${SCRIPT} Create config wg1.conf"
