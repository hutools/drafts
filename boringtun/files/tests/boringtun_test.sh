#!/usr/bin/env bash

# HELPER VARS
SCRIPT=$(readlink -f $0)
SCRIPTPATH=$(dirname $SCRIPT)
WGNODE="boringtun-ci-client"
WGNETWORK="boringtun_nw"
WGVOLUME="boringtun_vol"
WGVOLUMEMNT="/etc/wireguard"
DOCKER_IMAGE="boringtun-stage"

# If not in CI use latest from production
if [[ ! "${CI}" == "true"  ]]; then
  docker_username="kriegerse"
  DOCKER_IMAGE="boringtun"
  # DOCKER_IMAGE="boringtun-stage"
  BUILD_PRIMARY_TAG="latest"
  # BUILD_PRIMARY_TAG="commit-339032f7"
fi

# exit if no
if [[ -z ${boringtun_wg1_ci_client} ]]; then
  echo "ERROR Please set ENV variable 'boringtun_wg1_ci_client'"
  exit 1
fi


################################################################################
echo "========================================================================="
echo "Pulling docker image(s)"
echo "========================================================================="
echo "* Image: ${docker_username}/${DOCKER_IMAGE}:${BUILD_PRIMARY_TAG}"
docker pull ${docker_username}/${DOCKER_IMAGE}:${BUILD_PRIMARY_TAG}


echo "========================================================================="
echo "Create network ${WGNETWORK} for boringtun communication"
echo "========================================================================="
docker network create ${WGNETWORK}
docker network ls


echo "========================================================================="
echo "Create shared docker volume ${WGVOLUME} for data exchange"
echo "========================================================================="
docker volume create ${WGVOLUME}
docker volume ls


echo "========================================================================="
echo "Bootstrap ${WGNODE}"
echo "========================================================================="

echo "* Start Node ${WGNODE}"
docker run --rm -v ${WGVOLUME}:${WGVOLUMEMNT} \
   -v ${SCRIPTPATH}/boringtun_genconfig.sh:/usr/local/bin/boringtun_genconfig.sh \
   --hostname ${WGNODE} \
   --name ${WGNODE} \
   -e ${!boringtun_wg1_ci_client@}=${boringtun_wg1_ci_client} \
   -e ${!WGVOLUMEMNT@}=${WGVOLUMEMNT} \
   --network ${WGNETWORK} \
   --cap-add=NET_ADMIN \
   --device /dev/net/tun:/dev/net/tun \
   -dt ${docker_username}/${DOCKER_IMAGE}:${BUILD_PRIMARY_TAG}\
   /bin/bash
echo ""

echo "* Generating configs in ${WGNODE}"
docker exec -t ${WGNODE} /usr/local/bin/boringtun_genconfig.sh


echo "========================================================================="
echo "Starting boringtun"
echo "========================================================================="

echo "* Starting boringtun iface wg1 on ${WGNODE}"
docker exec -t ${WGNODE} wg-quick up wg1
echo ""

echo "* Waiting for establishing tunnel on ${WGNODE} (timout 120 seconds)"
docker exec -t ${WGNODE} /usr/bin/timeout 120s /bin/bash -c 'until wg show | grep endpoint > /dev/null  ; do wg show; echo WAITING for tunnel up; sleep 5; done ; wg show; echo TUNNEL is up'
echo ""

echo "* Waiting 5 seconds before starting tests"
docker exec -t ${WGNODE} /bin/sleep 5


echo "========================================================================="
echo "Interface information"
echo "========================================================================="

echo "* Interface information on ${WGNODE}"
docker exec -t ${WGNODE} ip addr show
echo ""

echo "* Interface route information on ${WGNODE}"
docker exec -t ${WGNODE} ip route show
docker exec -t ${WGNODE} ip route get 10.0.10.1
echo ""

echo "* Tunnel information on ${WGNODE}"
docker exec -t ${WGNODE} wg show
echo ""


echo "========================================================================="
echo "TEST: ping VPN-GW from ${WGNODE}"
echo "========================================================================="

docker exec -t ${WGNODE} ping -s 120  -W10 -c10 10.0.10.1
RESCODE=$?

if [ $RESCODE -eq 0 ]; then
  echo OKAY: Reached VPN-GW from ${WGNODE}.""
else
  echo "ERROR: Couldn't reach VPN-GW from ${WGNODE}."
  exit 1
fi


echo "========================================================================="
echo "Stats from boringtun"
echo "========================================================================="

echo "* Stats from ${WGNODE} iface wg1"
docker exec -t ${WGNODE} wg show wg1
docker exec -t ${WGNODE} wg show wg1 transfer
echo ""


echo "========================================================================="
echo "Stoping boringtun containers ${WGNODE}"
echo "========================================================================="

docker stop ${WGNODE}
docker volume rm --force ${WGVOLUME}
docker network rm ${WGNETWORK}
